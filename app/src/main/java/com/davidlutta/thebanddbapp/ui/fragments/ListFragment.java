package com.davidlutta.thebanddbapp.ui.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.davidlutta.thebanddbapp.R;
import com.davidlutta.thebanddbapp.database.BandDatabase;
import com.davidlutta.thebanddbapp.model.Band;
import com.davidlutta.thebanddbapp.ui.activities.DetailsActivity;

import java.util.List;

public class ListFragment extends Fragment {
    List<Band> bandList;

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        bandList = BandDatabase.getInstance(getContext()).getBands();
        LinearLayout layout = (LinearLayout) view;
        for (int i = 0; i < bandList.size(); i++) {
            Button button = new Button(getContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 10);
            button.setLayoutParams(layoutParams);
            Band band = BandDatabase.getInstance(getContext()).getBand(i + 1);
            final String bandName = band.getName();
            final String bandDescription = band.getDescription();
            button.setText(band.getName());
            button.setTag(band.getId());
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), DetailsActivity.class);
                    intent.putExtra("bandName", bandName);
                    intent.putExtra("bandDescription", bandDescription);
                    startActivity(intent);
                }
            });
            layout.addView(button);
        }
        return view;

        /*View view = inflater.inflate(R.layout.fragment_list, container, false);
        LinearLayout layout=(LinearLayout)view;
        List<Band> bandList = BandDatabase.getInstance(getContext()).getBands();
        for (int i=0;i<bandList.size();i++)
        {
            Button button=new Button(getContext());
            LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,0,0,10);
            button.setLayoutParams(layoutParams);

            //set the text to the band's name and the tag to the band ID
            Band band=BandDatabase.getInstance(getContext()).getBand(i+1);
            button.setText(band.getName());
            button.setTag(Integer.toString(band.getId()));
            //all the buttons have the same click listener

            button.setOnClickListener(buttonClickListener);
            //add the button to the LinearLayout
            layout.addView(button);

        }
        return view;*/
    }

    private View.OnClickListener buttonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };
}