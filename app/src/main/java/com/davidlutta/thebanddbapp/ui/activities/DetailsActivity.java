package com.davidlutta.thebanddbapp.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.davidlutta.thebanddbapp.R;
import com.davidlutta.thebanddbapp.model.Band;
import com.davidlutta.thebanddbapp.ui.fragments.DetailsFragment;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        if (getIntent().hasExtra("bandName") && getIntent().hasExtra("bandDescription")) {
            String bandName = getIntent().getStringExtra("bandName");
            String bandDescription = getIntent().getStringExtra("bandDescription");
            Bundle args = new Bundle();
            args.putString("bandName", bandName);
            args.putString("bandDescription", bandDescription);
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragment = fragmentManager.findFragmentById(R.id.detailsFragmentContainer);
            if (fragment == null) {
                fragment = new DetailsFragment();
                fragment.setArguments(args);
                fragmentManager.beginTransaction().add(R.id.detailsFragmentContainer, fragment).commit();
            }
        } else {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragment = fragmentManager.findFragmentById(R.id.detailsFragmentContainer);
            if (fragment == null) {
                fragment = new DetailsFragment();
                fragmentManager.beginTransaction().add(R.id.detailsFragmentContainer, fragment).commit();
            }
        }

    }
}