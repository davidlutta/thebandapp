package com.davidlutta.thebanddbapp.database;

import android.content.Context;
import android.content.res.Resources;

import com.davidlutta.thebanddbapp.R;
import com.davidlutta.thebanddbapp.model.Band;

import java.util.ArrayList;
import java.util.List;

public class BandDatabase {
    private static BandDatabase instance;
    private List<Band> mBands;

    private BandDatabase(Context context) {
        mBands = new ArrayList<>();
        Resources resources = context.getResources();
        String[] bands = resources.getStringArray(R.array.bands);
        String[] description = resources.getStringArray(R.array.descriptions);
        for (int i = 0; i < bands.length; i++) {
            mBands.add(new Band(i + 1, bands[i], description[i]));
        }
    }


    public static BandDatabase getInstance(Context context) {
        if (instance == null) {
            instance = new BandDatabase(context);
        }
        return instance;
    }

    public List<Band> getBands() {
        return mBands;
    }

    public Band getBand(int bandId) {
        if (mBands != null) {
            for (Band band : mBands) {
                if (band.getId() == bandId) {
                    return band;
                }
            }
        }
        return null;
    }
}
